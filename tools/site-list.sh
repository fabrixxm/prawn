#!/bin/bash

WIKI_URL="https://github.com/lobsters/lobsters.wiki.git"
WIKI_DIR="lobsters.wiki"

cd /tmp
[ -d "$WIKI_DIR" ] || git clone "$WIKI_URL"
cd "$WIKI_DIR"
git pull

echo
echo


SITESWD=`cat Home.md | sed -e '/## Cousin/,$d' | sed -n -e '/## Sister/,$p' | grep "^* " | sed "s/* \[\([^]]*\)\](\([^)]*\)).* - \(.*\)/\1;\2;\3/" | grep -v "^* "`
SITESND=`cat Home.md | sed -e '/## Cousin/,$d' | sed -n -e '/## Sister/,$p' | grep "^* " | sed "s/* \[\([^]]*\)\](\([^)]*\))[^-]*/\1;\2;/" | grep -v '\- '`
SITES=`echo -e "$SITESWD\n$SITESND" `

# $1 $2 $3: title url desc
function test {
  title=$1
  url=$2
  desc=$3
  echo "// $title ($url)"
  r=$(curl --max-time 5 "$url/hottest.json" 2>/dev/null| jq -r .[0].short_id_url 2>/dev/null)
  if [[ "$r" =~ "$url" ]]
  then
      echo "ListElement { url:'$url'; title:'$title'; desc:'$desc' }"
  else
    curl --max-time 5 "$url/hottest.json"
    echo
    if [[ "$url" =~ "www" ]]
    then
        url=${url/www./}
        test "$title" "$url" "$desc"
    fi
    if [[ "$url" =~ "http:" ]]
    then
        url=${url/http:/https:}
        test "$title" "$url" "$desc"
    fi
  fi
}

while IFS=';' read title url desc
do
  test "$title" "$url" "$desc"
done <<<"$SITES"

cd ..
#rm -fr "$WIKI_DIR"

