import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0

import "pages"
import "./utils.js" as Utils
ApplicationWindow
{
    readonly property int kStandardAnimationDuration: 200

    ConfigurationGroup {
        id: conf
        path: "/apps/harbour-prawn"

        property string site: "https://lobste.rs"
    }

    Constants {
        id: constants
    }


    initialPage: Component { StoriesList { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations


    Component.onCompleted: {
        pageStack.onCurrentPageChanged.connect(function(){
            var cbk =  pageStack.currentPage.onThisPage;
            if (cbk !== undefined) cbk();
        })
    }


    function doManageLink(link, isInternal) {
        console.log("link", link, "isInternal", isInternal);

        isInternal = (isInternal === undefined) ? Utils.isInternalStoryLink(conf.site, link) : isInternal;

        if (isInternal)  {
            // it's a link to a story!
            pageStack.push(Qt.resolvedUrl("pages/Story.qml"), {
                               story:{
                                   comments_url: link,
                                   comments: [],
                                   tags: [],
                                   title: "",
                                   description: "",
                                   score: "", comment_count: "",
                                   url: "",
                                   submitter_user: { username: "" },
                               },
                               replace: true
                           })
            return;
        }


        // it's an external url!
        // #TODO: link page
        Qt.openUrlExternally(link);

    }
}
