import QtQuick 2.0

JSONListModel {
    function transformJson(jo) {
        var t = [];
        for(var k = 0; k < jo.tags.length; k++) {
            t.push( { label: jo.tags[k] } );
        }
        jo.tags = t;
        return jo;
    }
}
