import QtQuick 2.0


JSONObjectData {
    id: storymodel

    signal storyReady

    ListModel {
        id: tagListModel
    }

    onObjChanged: {
        if (status > 199 && status < 400) {

            tagListModel.clear();
            for(var k = 0; k < obj.tags.length; k++) {
                tagListModel.append( { label: obj.tags[k] } );
            }
            obj.tags = tagListModel;
            storyReady()
        }
    }
}
