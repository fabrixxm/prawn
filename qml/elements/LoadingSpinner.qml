/*
    Copyright (C) 2018-2019 Fabio Comuni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see [http://www.gnu.org/licenses/].
*/

import QtQuick 2.0
import Sailfish.Silica 1.0


Item {
    id: loadingSpinner
    property Item model

    height:  100 + Theme.paddingLarge
    anchors {
        left: parent.left
        right: parent.right
        bottom: parent.bottom
        bottomMargin: -height
    }

    Rectangle {
        color: Theme.highlightBackgroundColor
        opacity: Theme.highlightBackgroundOpacity
        anchors.fill: parent;
    }

    BusyIndicator {
        id: indicator
        size: BusyIndicatorSize.Medium
        anchors.centerIn: parent
        running: false
    }


    states: State {
        name: "show";
        when: model.status === 0
        PropertyChanges { target: loadingSpinner; anchors.bottomMargin: 0  }
        PropertyChanges { target: indicator; running: true }
    }

    transitions: Transition {
        from: ""; to: "show"; reversible: true
        NumberAnimation { properties: "anchors.bottomMargin"; duration: kStandardAnimationDuration; easing.type: Easing.InOutQuad }
    }
}
