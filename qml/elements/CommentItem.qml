/*
    Copyright (C) 2018-2019 Fabio Comuni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see [http://www.gnu.org/licenses/].
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

import "../timeago.js"as TimeAgo

ListItem {
    id: delegate

    anchors {
        left: parent.left; right: parent.right
    }
    onClicked: console.log(height, column.height)

    contentHeight: column.height + (2 * Theme.paddingMedium)

    property int level: modelData.indent_level


    Repeater {
        model: level
        Rectangle {
            visible: index > 0
            color: constants.indentColors[index % 8]
            anchors {
                top: parent.top
                left: parent.left
                leftMargin:  Theme.paddingMedium * index
                bottom: parent.bottom
            }
            width: 1
        }
    }


    Column {
        id: column

        anchors {
            top:parent.top;
            left: parent.left; right: parent.right;
            topMargin: Theme.paddingMedium
            leftMargin: Theme.paddingMedium * (modelData.indent_level)
            rightMargin: Theme.paddingMedium
        }
        height: childrenRect.height

        spacing: Theme.paddingSmall

        Flow {
            spacing: Theme.paddingMedium

            Label {
                text: modelData.commenting_user.username
                color: delegate.highlighted ? constants.colorHi : constants.colorLight
                font.bold: true
            }

            Label {
                text: "· " + qsTr("%1 points").arg(modelData.score)
                color: delegate.highlighted ? constants.colorMid : constants.colorLight
                font.pixelSize: Theme.fontSizeSmall
            }

            Label {
                text: "· " + TimeAgo.format( modelData.created_at )
                color: delegate.highlighted ? constants.colorMid : constants.colorLight
                font.pixelSize: Theme.fontSizeSmall
            }
        }

        Label {
            anchors { left: parent.left; right: parent.right }
            textFormat: Text.RichText
            wrapMode: Text.WordWrap
            color: delegate.highlighted ? constants.colorHi : constants.colorLight
            text: constants.richtextStyle + modelData.comment
            onLinkActivated: doManageLink(link)
        }
    }
}
