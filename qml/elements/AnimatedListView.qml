/*
    Copyright (C) 2018-2019 Fabio Comuni

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see [http://www.gnu.org/licenses/].
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

SilicaListView {
    add: Transition {
        ParallelAnimation {
            NumberAnimation {
                property: "scale"
                from: 0.0
                to: 1.0
                duration: kStandardAnimationDuration
                easing.type: Easing.OutCubic
            }
            NumberAnimation {
                property: "height"
                from: 0
                duration: kStandardAnimationDuration
                easing.type: Easing.OutCubic
            }
            NumberAnimation {
                property: "opacity"
                from: 0.0
                to: 1.0
                duration: kStandardAnimationDuration
                easing.type: Easing.OutCubic
            }
        }
    }
    remove: Transition {
        ParallelAnimation {
            NumberAnimation {
                property: "scale"
                from: 1.0
                to: 0.0
                duration: kStandardAnimationDuration
                easing.type: Easing.OutCubic
            }
            NumberAnimation {
                property: "height"
                to: 0
                duration: kStandardAnimationDuration
                easing.type: Easing.OutCubic
            }
            NumberAnimation {
                property: "opacity"
                from: 1.0
                to: 0.0
                duration: kStandardAnimationDuration
                easing.type: Easing.OutCubic
            }
        }
    }
}

