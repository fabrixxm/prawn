import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../elements"


Dialog {
    id: dialog
    property string site
    canAccept: false

    SilicaListView {
        id: comments
        anchors.fill: parent

        model: ListModel {
            ListElement { url: "https://lobste.rs"; title: "Lobste.rs"; desc: "Computing-focused community"}
            // sister sites

            // Barnacles (https://www.barnacl.es)
            // Barnacles (https://barnacl.es)
            ListElement { url:'https://barnacl.es'; title:'Barnacles'; desc:'Business' }
            // Le Journal du Hacker (https://www.journalduhacker.net/)
            ListElement { url:'https://www.journalduhacker.net/'; title:'Le Journal du Hacker'; desc:'Computer Science (fr)' }
            // CryptoBuzz (https://cryptobuzz.io/)
            ListElement { url:'https://cryptobuzz.io/'; title:'CryptoBuzz'; desc:'Cryptocurrency' }
            // Kind and Green News (http://news.kindandgreenworld.com/)
            // Kind and Green News (https://news.kindandgreenworld.com/)
            ListElement { url:'https://news.kindandgreenworld.com/'; title:'Kind and Green News'; desc:'Biology' }
            // tilde news (https://tilde.news/)
            ListElement { url:'https://tilde.news/'; title:'tilde news'; desc:'[tildeverse](https://tildeverse.org) links: unix, small web, decentralization' }
            // Standard (https://std.bz/)
            ListElement { url:'https://std.bz/'; title:'Standard'; desc:'Cryptocurrency' }
            // Gambe.ro (https://gambe.ro)
            ListElement { url:'https://gambe.ro'; title:'Gambe.ro'; desc:'Software development (it)' }
            // Middlebit (https://middlebit.com/)
            // curl: (60) SSL certificate problem: unable to get local issuer certificate
            // ACRAB (https://acrab.isi.jhu.edu/)
            // curl: (60) SSL certificate problem: unable to get local issuer certificate
            // V2ETH (https://v2eth.com)
            ListElement { url:'https://v2eth.com'; title:'V2ETH'; desc:'Cryptocurrency (Ethereum)' }
            // Hipppo (https://hipppo.fm)
            ListElement { url:'https://hipppo.fm'; title:'Hipppo'; desc:'Hip-hop' }
        }

        header: headercomponent
        Component {
            id: headercomponent
            Column {
                id: column
                width: parent.width
                height: childrenRect.height
                spacing: Theme.paddingMedium
                PageHeader {
                    id: pageHeader
                    title: qsTr("Change site")
                }
                TextField {
                    id: customUrl
                    anchors { left: parent.left; right: parent.right }
                    placeholderText: qsTr("Custom site url...")
                    text: dialog.site
                    onTextChanged: {
                        dialog.canAccept = text.length > 0
                        dialog.site = text
                    }
                    inputMethodHints: Qt.ImhNoPredictiveText
                    // TODO: minimal url validation?
                    // Only allow Enter key to be pressed when text has been entered
                    EnterKey.enabled: text.length > 0
                    // When Enter key is pressed, accept the dialog
                    EnterKey.onClicked: dialog.accept()
                }
            }
        }


        delegate: ListItem {
            contentHeight: column.height + (2* Theme.paddingMedium)
            Column {
                anchors {
                    left: parent.left;
                    right: parent.right;
                    top: parent.top
                    margins: Theme.paddingMedium
                }
                id: column
                height: childrenRect.height

                Label {
                    anchors { left: parent.left; right: parent.right; }
                    color: constants.colorLight
                    font.bold: true
                    text: title
                }
                Label {
                    anchors { left: parent.left; right: parent.right; }
                    color: constants.colorMid
                    text: url
                }
                Label {
                    anchors { left: parent.left; right: parent.right; }
                    color: constants.colorLight
                    text: desc
                }
            }
            onClicked: {
                dialog.canAccept = true;
                dialog.site = url
                dialog.accept()
            }
        }

        VerticalScrollDecorator {}
    }


/*    onDone: {
        if (result == DialogResult.Accepted) {
            if (headercomponent.text !== "") {
                dialog.site = headercomponent.text
            }
        }
    }
*/
}
