import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    id: dialog
    property string section

    Column {
        width: parent.width

        DialogHeader { }

        TextField {
            id: tagField
            width: parent.width
            placeholderText: qsTr("Filter by tag")
            inputMethodHints: Qt.ImhNoPredictiveText
        }

        ListItem {
            anchors { left: parent.left; right: parent.right; margins: Theme.paddingMedium }
            Label {
                anchors { top: parent.top; left: parent.left; right: parent.right; margins: Theme.paddingMedium }
                text: qsTr("Hottest")
            }
            onClicked: {
                dialog.section = "/hottest"
                dialog.accept()
            }
        }

        ListItem {
            anchors { left: parent.left; right: parent.right; margins: Theme.paddingMedium }
            Label {
                anchors { top: parent.top; left: parent.left; right: parent.right; margins: Theme.paddingMedium }
                text: qsTr("Newest")
            }
            onClicked: {
                dialog.section = "/newest"
                dialog.accept()
            }
        }
    }

    onDone: {
        if (result == DialogResult.Accepted) {
            if (tagField.text !== "") {
                section = "/t/" + tagField.text
            }
        }
    }

}
