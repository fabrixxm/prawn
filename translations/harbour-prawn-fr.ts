<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>CommentItem</name>
    <message>
        <source>%1 points</source>
        <translation>%1 points</translation>
    </message>
</context>
<context>
    <name>LinkPage</name>
    <message>
        <source>Open URL</source>
        <translation>Ouvrir l&apos;URL</translation>
    </message>
    <message>
        <source>Open in browser</source>
        <translation>Ouvrir avec le navigateur</translation>
    </message>
    <message>
        <source>Copy URL</source>
        <translation>Copier l&apos;URL</translation>
    </message>
    <message>
        <source>Copy link</source>
        <translation>Copier le lien</translation>
    </message>
</context>
<context>
    <name>Section</name>
    <message>
        <source>Filter by tag</source>
        <translation>Filtrer par étiquette</translation>
    </message>
    <message>
        <source>Hottest</source>
        <translation>Plus actuels</translation>
    </message>
    <message>
        <source>Newest</source>
        <translation>Plus récents</translation>
    </message>
</context>
<context>
    <name>Sites</name>
    <message>
        <source>Change site</source>
        <translation>Changer de site</translation>
    </message>
    <message>
        <source>Custom site url...</source>
        <translation type="unfinished">URL de site personnalisée</translation>
    </message>
</context>
<context>
    <name>StoriesList</name>
    <message>
        <source>No items</source>
        <translation>Aucun élément</translation>
    </message>
    <message>
        <source>Site</source>
        <translation>Site</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Actualiser</translation>
    </message>
    <message>
        <source>Section</source>
        <translation>Section</translation>
    </message>
</context>
<context>
    <name>Story</name>
    <message>
        <source>Comments</source>
        <translation>Commentaires</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Actualiser</translation>
    </message>
    <message>
        <source>No comments</source>
        <translation>Aucun commentaire</translation>
    </message>
</context>
<context>
    <name>StoryHeader</name>
    <message>
        <source>sent %1 by %2</source>
        <translation>envoyé %1 par %2</translation>
    </message>
    <message>
        <source>%1 points - %2 comments</source>
        <translation>%1 points - %2 commentaires</translation>
    </message>
</context>
<context>
    <name>timeago</name>
    <message>
        <source>second</source>
        <translation>segonde</translation>
    </message>
    <message>
        <source>minute</source>
        <translation>minutes</translation>
    </message>
    <message>
        <source>hour</source>
        <translation>heure</translation>
    </message>
    <message>
        <source>day</source>
        <translation>jour</translation>
    </message>
    <message>
        <source>week</source>
        <translation>semaine</translation>
    </message>
    <message>
        <source>month</source>
        <translation>mois</translation>
    </message>
    <message>
        <source>year</source>
        <translation>année</translation>
    </message>
    <message>
        <source>seconds</source>
        <translation>segondes</translation>
    </message>
    <message>
        <source>minutes</source>
        <translation>minutes</translation>
    </message>
    <message>
        <source>hours</source>
        <translation>heures</translation>
    </message>
    <message>
        <source>days</source>
        <translation>jours</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>semaines</translation>
    </message>
    <message>
        <source>months</source>
        <translation>mois</translation>
    </message>
    <message>
        <source>years</source>
        <translation>années</translation>
    </message>
    <message>
        <source>just now</source>
        <translation>à l&apos;instant</translation>
    </message>
    <message>
        <source>right now</source>
        <translation>maintenant</translation>
    </message>
    <message>
        <source>%1 %2 ago</source>
        <extracomment>%number %unit ago (eg. 2 days ago)</extracomment>
        <translation>il y a %1 %2</translation>
    </message>
    <message>
        <source>in %1 %2</source>
        <extracomment>in %number %unit (eg. in 2 days)</extracomment>
        <translation>dans %1 %2</translation>
    </message>
</context>
</TS>
