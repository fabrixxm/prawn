<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CommentItem</name>
    <message>
        <source>%1 points</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LinkPage</name>
    <message>
        <source>Open URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy link</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Section</name>
    <message>
        <source>Filter by tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hottest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Newest</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Sites</name>
    <message>
        <source>Change site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom site url...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StoriesList</name>
    <message>
        <source>No items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Section</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Story</name>
    <message>
        <source>Comments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No comments</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StoryHeader</name>
    <message>
        <source>sent %1 by %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 points - %2 comments</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>timeago</name>
    <message>
        <source>second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>years</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>just now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>right now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 %2 ago</source>
        <extracomment>%number %unit ago (eg. 2 days ago)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>in %1 %2</source>
        <extracomment>in %number %unit (eg. in 2 days)</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
